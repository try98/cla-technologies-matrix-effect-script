#__________________________________________________________
#|CLA Technologies simple matrix effect using bash script.|
#|For a more retro effect install Cool Retro Term.        |
#|________________________________________________________|
#

#_________________________________________________________________________
#|Special thanks to Shadab Mohammad and Victor Hugo for                  |
#|making this script possible.                                           |
#|https://github.com/victorhundo/                                        |
#|https://github.com/shadabshaukat                                       |
#|_______________________________________________________________________|


#_____________________________________________________________
#|I am not responsible if any editing or executing of this   |
#|script causes damage to your computer or operating system  |
#|___________________________________________________________|
#

[code language="bash"]
#!/bin/bash

clear # clear the terminal window
virtcent=$((`tput lines`/2)) # Set terminal text position (top/center/bottom)
horcent=$((`tput cols`/2-10))
tput setf 6 #Set color
tput cup $virtcent $horcent && echo "                              " #Set text
sleep 1 # "Pause for 1 second"
tput cup $virtcent $horcent && echo "Welcome to CLA Technologies."
sleep 3
tput cup $virtcent $horcent && echo "                              "
sleep 1
tput cup $virtcent $horcent && echo " Entering the matrix"
sleep 2
tput cup $virtcent $horcent && echo "                         "
sleep 1
tput cup $virtcent $horcent && echo " Entering the matrix"
sleep 2
tput cup $virtcent $horcent && echo "                         "
sleep 1
tput cup $virtcent $horcent && echo "Connecting. "
sleep .1
tput cup $virtcent $horcent && echo "Connecting.. "
sleep .1
tput cup $virtcent $horcent && echo "Connecting... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting.... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting..... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting...... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting....... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting....... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting....... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting........ "
sleep .1
tput cup $virtcent $horcent && echo "Connecting......... "
sleep .1
tput cup $virtcent $horcent && echo "Connecting.........."
sleep 1
tput cup $virtcent $horcent && echo "     CONNECTED!     "
sleep 1
clear

#Matrix code starts here
N_LINE=$(( $(tput lines) - 1));
N_COLUMN=$(tput cols);

function get_char {
    RANDOM_U=$(echo $(( (RANDOM % 9) + 0)));
    RANDOM_D=$(echo $(( (RANDOM % 9) + 0)));

    #https://unicode-table.com/en/#kangxi-radicals
    CHAR_TYPE="\u04"

    printf "%s" "$CHAR_TYPE$RANDOM_D$RANDOM_U";
}


function cursor_position {
    echo "\033[$1;${RANDOM_COLUMN}H";
}

function write_char {
    CHAR=$(get_char);
    print_char $1 $2 $CHAR
}

function erase_char {
    CHAR="\u0020" #Space char
    print_char $1 $2 $CHAR
}

function print_char {
    CURSOR=$(cursor_position $1);
    echo -e "$CURSOR$2$3";
}


function draw_line {
    RANDOM_COLUMN=$[RANDOM%N_COLUMN];
    RANDOM_LINE_SIZE=$(echo $(( (RANDOM % $N_LINE) + 1)));
    SPEED=0.05

    COLOR="\033[32m"; #GREEN
    COLOR_HEAD="\033[37m"; #WHITE

    #Draw Line
    for i in $(seq 1 $N_LINE ); do
        write_char $[i-1] $COLOR;
        write_char $i $COLOR_HEAD;
        sleep $SPEED;
        if [ $i -ge $RANDOM_LINE_SIZE ]; then
            erase_char $[i-RANDOM_LINE_SIZE];
        fi;
    done;

    #Erase Line
    for i in $(seq $[i-$RANDOM_LINE_SIZE] $N_LINE); do
        erase_char $i
        sleep $SPEED;
    done
}

function matrix {
    tput setab 000 #Background Black
    clear
    while true; do
        draw_line & #Parallel
        sleep 0.5;
    done
}

matrix;

[/code]

# Matrix effect bash script

[![Platform: GNU/Linux](https://img.shields.io/badge/platform-GNU/Linux-blue.svg)](https://www.kernel.org/linux.html) [![License: GPL v2](https://img.shields.io/badge/License-GPLv2-green.svg)](https://www.gnu.org/licenses/gpl-2.0)

Nice little shell script for a matrix effect on a linux terminal
Before executing make sure that in the file properties the "is executable" option is ticked

CLI metheod:
```bash
chmod +x <filename>
```


# Optional

For a more "aesthetic" and retro effect install cool retro term -> https://github.com/Swordfish90/cool-retro-term

Ubuntu 22.10:
```bash
sudo apt install cool-retro-term
```

# Image

![open image](/welcome.png)

# Forks

This project was made possible by forks from https://github.com/victorhundo/matrix-script/blob/master/matrix.sh and https://easyoradba.com/2016/07/18/linux-matrix-effects-shell-script/
